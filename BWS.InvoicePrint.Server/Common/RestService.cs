﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;

using TheOne.Diagnostics;
using RestSharp;
using Newtonsoft.Json.Linq;
using RestSharp.Serializers.Newtonsoft.Json;

namespace BWS.InvoicePrint.Server.Common
{
    public class RestService
    {
        static IFoxLog _log = FoxLogManager.GetLogger("BWS.InvoicePrint.Server");

        public static IRestResponse CallRestAPI(string url, Method method, object obj)
        {
            IRestResponse retVal = null;

            try
            {
                var client = new RestClient(url);

                RestSharp.RestRequest request = new RestSharp.RestRequest(method);
                request.RequestFormat = DataFormat.Json;
                request.JsonSerializer = new NewtonsoftJsonSerializer();
                request.AddJsonBody(obj);

                retVal = client.Execute(request);                
            }
            catch (Exception ex)
            {
                _log.ErrorFormat("CallRestAPI - {0}", ex.Message);
                _log.Error(ex);
            }

            return retVal;
        }
    }
}