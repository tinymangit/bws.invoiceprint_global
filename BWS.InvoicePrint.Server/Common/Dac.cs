﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TheOne.Data.SqlClient;
using TheOne.Transactions;
using TheOne.Diagnostics;
using Newtonsoft.Json;

namespace BWS.InvoicePrint.Server.Common
{
    public class CommonDac : FoxDacBase
    {
        IFoxLog _log = FoxLogManager.GetLogger("BWS.InvoicePrint.Server");
        
        public DataTable GetInvoiceOrdNatGbList(string companyCode, string langCode, string packNumber, string dlvType, string ordNums)
        {
            DataTable retVal = null;

            try
            {
                FoxSqlParameterCollection parameters = new FoxSqlParameterCollection();
                parameters.AddWithValue("@CMPNY_CD", SqlDbType.VarChar, 10, companyCode);
                parameters.AddWithValue("@LANG_GB", SqlDbType.VarChar, 10, langCode);
                parameters.AddWithValue("@PACKING_NUM", SqlDbType.VarChar, 12, packNumber);
                parameters.AddWithValue("@DLV_TYPE", SqlDbType.VarChar, 10, dlvType);
                parameters.AddWithValue("@ORD_NUMS", SqlDbType.NVarChar, ordNums);

                DataSet ds = this.DbAccess.ExecuteSpDataSet("dbo.BSP_RL_API_INVOICE_ORDNAT_INFO_S", parameters);
                if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count > 0)
                {
                    retVal = ds.Tables[0];
                }
            }
            catch (Exception ex)
            {
                _log.ErrorFormat("{0} - {1}", this.GetType(), ex.Message);
                throw ex;
            }

            return retVal;
        }
        
        /// <summary>
        /// 출력 과정 중에 처리 상태를 저장
        /// </summary>
        /// <param name="companyCode"></param>
        /// <param name="langCode"></param>
        /// <param name="packNumber"></param>
        /// <param name="prtState"></param>
        /// <param name="etcMsg"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int SavePrintStateLog(string companyCode, string langCode, string packNumber, int prtState, string etcMsg, string userId)
        {
            int retVal = -1;
            try
            {
                FoxSqlParameterCollection parameters = new FoxSqlParameterCollection();
                parameters.AddWithValue("@CMPNY_CD", SqlDbType.VarChar, 10, companyCode);
                parameters.AddWithValue("@LANG_GB", SqlDbType.VarChar, 10, langCode);
                parameters.AddWithValue("@PACKING_NUM", SqlDbType.VarChar, 12, packNumber);
                parameters.AddWithValue("@STATE_GB", SqlDbType.Int, prtState);
                parameters.AddWithValue("@ETC_MESSAGE", SqlDbType.NVarChar, etcMsg);
                parameters.AddWithValue("@USR_ID", SqlDbType.VarChar, 6, userId);

                retVal = this.DbAccess.ExecuteSpNonQuery("dbo.BSP_RL_API_INVOICE_STATE_LOG_CU", parameters);
            }
            catch (Exception ex)
            {
                _log.ErrorFormat("{0} - {1}", this.GetType(), ex.Message);
                throw ex;
            }

            return retVal;
        }        
    }
}