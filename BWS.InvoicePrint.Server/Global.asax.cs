﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

using Microsoft.AspNet.SignalR;

namespace BWS.InvoicePrint.Server
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            //GlobalHost.Configuration.DisconnectTimeout = TimeSpan.FromHours(1);
            GlobalHost.Configuration.DisconnectTimeout = TimeSpan.FromSeconds(600);
            GlobalHost.Configuration.KeepAlive = TimeSpan.FromSeconds(10);
            GlobalHost.Configuration.MaxIncomingWebSocketMessageSize = 500000;
        }
    }
}