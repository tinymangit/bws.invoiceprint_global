﻿using System;
using System.Threading.Tasks;
using System.Web.Cors;
using Owin;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.AspNet.SignalR;

[assembly: OwinStartup(typeof(BWS.InvoicePrint.Server.StartUp))]

namespace BWS.InvoicePrint.Server
{
    public class StartUp
    {
        public void Configuration(IAppBuilder app)
        {

            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888

            app.Map("/signalr", map =>
            {
                map.UseCors(CorsOptions.AllowAll);

                var corsPolicy = new CorsPolicy
                {
                    AllowAnyHeader = true,
                    AllowAnyMethod = true
                };

                // 특정 origin 만 제한 
                //corsPolicy.Origins.Add("http://localhost:10457");

                map.UseCors(
                    new CorsOptions
                    {
                        PolicyProvider = new CorsPolicyProvider
                        {
                            PolicyResolver = r => Task.FromResult(corsPolicy)
                        }
                    });

                var hubConfiguration = new HubConfiguration
                {
                    EnableDetailedErrors = true
                };

                map.RunSignalR(hubConfiguration);
            });

            app.MapSignalR();
        }
    }
}
