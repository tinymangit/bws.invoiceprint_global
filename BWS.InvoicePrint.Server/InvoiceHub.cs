﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System.Text.RegularExpressions;

using TheOne.Configuration;
using TheOne.Diagnostics;
using Newtonsoft.Json;

using BWS.InvoicePrint.Core;
using BWS.InvoicePrint.Server.Common;
using RestSharp;
using System.Net;
using System.Configuration;
using System.IO;
using System.Text;

namespace BWS.InvoicePrint.Server
{
    [HubName("Invoice")]
    public class InvoiceHub : Hub
    {
        private static IFoxLog _log = FoxLogManager.GetLogger("BWS.InvoicePrint.Server");

        private static Dictionary<string, SubscribeLockerMessage> lockerList = new Dictionary<string, SubscribeLockerMessage>();
        private static Dictionary<string, SubscribeAgentMessage> printAgentList = new Dictionary<string, SubscribeAgentMessage>();
        private readonly string invoiceRestApiUrl = string.Empty;
        private readonly string cjinvoiceAddrSetUrl = string.Empty;
        public InvoiceHub()
        {
           
        }
        /// <summary>
        /// 보관함에서 접속 시 호출
        /// </summary>
        /// <param name="message"></param>
        public SubscribeLockerMessage SubscribeLocker(SubscribeLockerMessage message)
        {
            string conLogMsg = "Locker 접속 시도 : {0} / 결과 : {1}{2}";
            SubscribeLockerMessage retVal = message;

            string connectionId = Context.ConnectionId;

            try
            {
                string lockerName = message.LockerName;
                // 호출 하려는 PrintAgent 명
                string printAgentName = message.PrintAgentName;

                if (string.IsNullOrEmpty(lockerName))
                    throw new Exception("lockerName 값은 null 일 수 없습니다.");

                if (lockerList.Values.Count(t => t.LockerName == lockerName) != 0)
                    throw new Exception("lockerName 값이 이미 존재 합니다.");

                if (string.IsNullOrEmpty(printAgentName))
                    throw new Exception("PrintAgent 값은 null 일 수 없습니다.");

                // 접속 해 있는 PrintAgent가 존재 하지 않을 경우
                if (printAgentList.Values.Count(t => t.PrintAgentName == printAgentName) == 0)
                    throw new Exception(string.Format("지정된 PrintAgent가 존재하지 않습니다. - AgentName : {0}", message.PrintAgentName));
                
                if (!lockerList.ContainsKey(connectionId))
                    lockerList.Add(connectionId, message);
                else
                {
                    lockerList[connectionId] = message;
                }
            }
            catch (Exception ex)
            {
                retVal.IsSuccess = false;
                retVal.ErrorMessage = string.Format("SubscribeLocker Error. - Error Message : {0}", ex.Message);

                _log.ErrorFormat(conLogMsg, connectionId, "실패", $" / {ex.Message}");
                return retVal;
            }

            _log.InformationFormat(conLogMsg, connectionId, "성공", string.Empty);

            retVal.IsSuccess = true;
            return retVal;
        }
        /// <summary>
        /// InvoicePrint Agent 접속 시 호출 됨.
        /// </summary>
        /// <param name="message"></param>
        public SubscribeAgentMessage SubscribeAgent(SubscribeAgentMessage message)
        {
            string conLogMsg = "Agent 접속 시도 : {0} / 결과 : {1}{2}";

            SubscribeAgentMessage retVal = message;
            
            string connectionId = Context.ConnectionId;

            try
            {                
                string existKey = printAgentList.FirstOrDefault(t => t.Value.PrintAgentName == message.PrintAgentName).Key;
                if (string.IsNullOrEmpty(existKey) == false)
                {
                    printAgentList.Remove(existKey);
                }

                if (!printAgentList.ContainsKey(connectionId))
                {
                    printAgentList.Add(connectionId, message);
                }
                else
                    printAgentList[connectionId] = message;
            }
            catch (Exception ex)
            {
                retVal.IsSuccess = false;
                retVal.ErrorMessage = string.Format("SubscribeAgent Error. - Error Message : {0}", ex.Message);

                _log.ErrorFormat(conLogMsg, connectionId, "실패", $" / {ex.Message}");
                return retVal;
            }

            _log.InformationFormat(conLogMsg, connectionId, "성공", string.Empty);

            retVal.IsSuccess = true;
            return retVal;
            
        }
        /// <summary>
        /// 보관함에서 송장 출력을 요청 할 때 호출되는 함수
        /// </summary>
        /// <param name="printInvoiceMessage"></param>
        public void PrintInvoice(PrintInvoiceMessage printInvoiceMessage)
        {
            string strNatGb = string.Empty;
            string strDLVR_MTD_GB = string.Empty;

            // 호출 한 보관함 웹 페이지
            var caller = Clients.Caller;
            string connectionId = Context.ConnectionId;
            // 웹페이지에서 인쇄 요청 해야 하는 PrintAgent Name
            string printAgentName = lockerList.FirstOrDefault(t => t.Key == connectionId).Value.PrintAgentName;
            string printAgentConnectionId = printAgentList.FirstOrDefault(t => t.Value.PrintAgentName == printAgentName).Key;

            _log.InformationFormat("{0} - {1}", this.GetType(), printInvoiceMessage.ToString());

            // 최초 요청 받으면 로그 저장 (UserId 컬럼은 짧기 때문에 메모부분에 LockerName 저장)
            this.SavePrintLog(printInvoiceMessage, PrintState.PRT_FROM_LOCKER, $"[LOCKER 출력 요청] {printInvoiceMessage.LockerName} - {printAgentConnectionId}", "SERVER");

            try
            {
                using (CommonDac dac = new CommonDac())
                {
                    string strOrderNums = string.Empty;

                    // 주문번호 배열(, 구분자)
                    for (int i = 0; i < printInvoiceMessage.OrderNumbers.Count; i++)
                    {
                        strOrderNums += "," + printInvoiceMessage.OrderNumbers[i].OrderNo;
                    }

                    if (printInvoiceMessage.InvType == "YAMATO")
                        strOrderNums = strOrderNums.Substring(1);
                    else
                        strOrderNums = string.Empty;

                    printInvoiceMessage.InvoiceOrdNatGbList = dac.GetInvoiceOrdNatGbList(printInvoiceMessage.ComapnyCode, printInvoiceMessage.LanguageCode, printInvoiceMessage.PackingNumber, printInvoiceMessage.InvType, strOrderNums);

                    if (printInvoiceMessage.InvoiceOrdNatGbList == null ||
                        (printInvoiceMessage.InvoiceOrdNatGbList != null && printInvoiceMessage.InvoiceOrdNatGbList.Rows.Count == 0))
                        //throw new Exception(string.Format("해당 PackingNumber로 DB에서 정보를 조회 할 수 없습니다. - {0}", printInvoiceMessage.PackingNumber));
                        throw new Exception(string.Format("해당 PackingNumber로 DB에서 정보를 조회 할 수 없습니다. - {0}", printInvoiceMessage.InvType + "/" + strOrderNums));

                    //strNatGb = printInvoiceMessage.InvoiceOrdNatGbList.Rows[0]["ORD_NAT_GB"].ToString();
                    strDLVR_MTD_GB = printInvoiceMessage.InvoiceOrdNatGbList.Rows[0]["DLVR_MTD_GB"].ToString();

                    //if (strDLVR_MTD_GB == "DLV_P41" || strDLVR_MTD_GB == "DLV_P42") // 사가와
                    //{
                    try
                    {
                        printInvoiceMessage.IsSuccess = true;

                        // Agent 호출 전 로그 저장
                        this.SavePrintLog(printInvoiceMessage, PrintState.PRT_REQ_AGT, $"[Agent 호출] {printInvoiceMessage.LockerName} - {printAgentConnectionId}", "SERVER");

                        // PrintInvoice Agent 호출
                        Clients.Client(printAgentConnectionId).printInvoice(printInvoiceMessage);
                    }
                    catch (Exception ex)
                    {
                        _log.Error(ex);

                        printInvoiceMessage.ErrorMessage = ex.Message;
                        printInvoiceMessage.IsSuccess = false;

                        // 처리 중 오류 저장
                        this.SavePrintLog(printInvoiceMessage, PrintState.PRT_REQ_AGT, "[에러발생] " + ex.ToString(), "SERVER");

                        // 락커 클라이언트에 실패에 대한 메시지 전달
                        string lockerConnectionId = lockerList.FirstOrDefault(t => t.Value.LockerName == printInvoiceMessage.LockerName).Key;
                        Clients.Client(lockerConnectionId).printInvoiceResult(printInvoiceMessage);
                    }
                    //}                    
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex);

                printInvoiceMessage.ErrorMessage = ex.Message;
                printInvoiceMessage.IsSuccess = false;

                // 처리 중 오류 저장
                this.SavePrintLog(printInvoiceMessage, PrintState.PRT_REQ_AGT, "[에러발생] " + ex.ToString(), "SERVER");

                // 락커 클라이언트에 실패에 대한 메시지 전달
                string lockerConnectionId = lockerList.FirstOrDefault(t => t.Value.LockerName == printInvoiceMessage.LockerName).Key;
                Clients.Client(lockerConnectionId).printInvoiceResult(printInvoiceMessage);
            }
        }

        /// <summary>
        /// PrintInvoice Agent 에서 송장 출력 후 결과값 리턴
        /// 보관함 웹으로 결과값 다시 포워딩
        /// </summary>
        /// <param name="printInvoiceMessage"></param>
        public void PrintInvoiceResult(PrintInvoiceMessage printInvoiceMessage)
        {
            string etcMsg = string.Empty;

            if (printInvoiceMessage.IsSuccess)
            {
                // 인쇄 성공
                etcMsg = $"[인쇄 완료]";
            }
            else
            {
                // 인쇄 실패
                etcMsg = $"[인쇄 실패] {printInvoiceMessage.ErrorMessage}";
            }

            // 인쇄 결과 관계없이 출력 처리 완료 로그
            this.SavePrintLog(printInvoiceMessage, PrintState.PRT_COMPLETE, etcMsg, "SERVER");

            string lockerConnectionId = lockerList.FirstOrDefault(t => t.Value.LockerName == printInvoiceMessage.LockerName).Key;
            Clients.Client(lockerConnectionId).printInvoiceResult(printInvoiceMessage);
        }

        /// <summary>
        /// SignalR 통해 출력 진행 사항 로그 저장 (AGENT로부터 호출됨)
        /// </summary>
        /// <param name="printInvoiceMessage"></param>
        /// <param name="prtState"></param>
        /// <param name="etcMsg"></param>
        public void PrintInvoiceSaveLog(PrintInvoiceMessage printInvoiceMessage, int prtState, string etcMsg)
        {
            this.SavePrintLog(printInvoiceMessage, (PrintState)prtState, etcMsg, "AGENT");
        }

        /// <summary>
        /// 출력 처리 중 로그 저장 (로그 저장으로 인해 출력 과정이 종료되지 않도록 예외 무시)
        /// </summary>
        /// <param name="printInvoiceMessage"></param>
        /// <param name="prtState"></param>
        /// <param name="etcMsg"></param>
        /// <param name="userId"></param>
        private void SavePrintLog(PrintInvoiceMessage printInvoiceMessage, Core.PrintState prtState, string etcMsg, string userId)
        {
            // 최초 보관함에서 출력 요청 받은 것에 대한 로그 저장
            try
            {
                using (CommonDac dac = new CommonDac())
                {
                    dac.SavePrintStateLog(
                        printInvoiceMessage.ComapnyCode, printInvoiceMessage.LanguageCode, printInvoiceMessage.PackingNumber,
                        (int)prtState, etcMsg, userId);
                }
            }
            catch (Exception ex)
            {
                _log.ErrorFormat("{0} [상태 로그 저장 중 에러] - {1}", this.GetType(), ex.ToString());
            }
        }

        public override Task OnConnected()
        {
            // Trace.WriteLine(string.Format("Connected: {0}", Context.ConnectionId));

            return base.OnConnected();
        }

        public override Task OnReconnected()
        {
            return base.OnReconnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            var caller = Clients.Caller;
            string connectionId = Context.ConnectionId;

            if (printAgentList.ContainsKey(connectionId))
            {
                // 보관함 웹페이지에 PrintAgent 접속 끊김에 대한 알림
                string printAgentName = printAgentList.FirstOrDefault(t => t.Key == connectionId).Value.PrintAgentName;
                if (string.IsNullOrEmpty(printAgentName) == false)
                {
                    var lokerList = lockerList.Where(t => t.Value.PrintAgentName == printAgentName).ToList();
                    foreach (var locker in lockerList)
                    {
                        string msg = string.Format("PrintAgent 접속이 끊겼습니다. - {0}", printAgentName);

                        Clients.Client(locker.Key).printAgentDisconnected(msg);
                    }
                }

                printAgentList.Remove(connectionId);
            }
            if (lockerList.ContainsKey(connectionId))
                lockerList.Remove(connectionId);
            
            return base.OnDisconnected(stopCalled);
        }
    }
}