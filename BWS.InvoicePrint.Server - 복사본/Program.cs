﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using TheOne.Configuration;
using TheOne.Diagnostics;
using Topshelf;

using BWS.InvoicePrint.Server.Common;

namespace BWS.InvoicePrint.Server
{
    class Program
    {
        static IFoxLog logger = FoxLogManager.GetLogger("BWS.InvoicePrint.Server");

        static void Main(string[] args)
        {
            GlobalExceptionsHandler.AddHandler();

            string serviceName = FoxConfigurationManager.AppSettings["SERVICE_NAME"];
            string displayName = FoxConfigurationManager.AppSettings["DISPLAY_NAME"];
            string description = FoxConfigurationManager.AppSettings["DESCRIPTION"];

            //var host = HostFactory.New(x =>
            //{
            //    //x.EnableDashboard();
            //    x.Service<SignalRServer>(s =>
            //    {
            //        s.ConstructUsing(() => new SignalRServer());
            //        s.WhenStarted(tc =>
            //        {
            //            tc.StartService();
            //        });
            //        s.WhenStopped(tc => tc.StopService());
            //    });

            //    x.RunAsLocalSystem();

            //    x.SetServiceName(serviceName);
            //    x.SetDisplayName(displayName);
            //    x.SetDescription(description);
            //    x.StartAutomatically();

            //    x.BeforeInstall(() =>
            //    {
            //        logger.Information("Service before install");
            //    });

            //    x.AfterInstall(() =>
            //    {
            //        logger.Information("Service after install");
            //    });
            //});

            //host.Run();

            Application.Run(new FrmSignalRTest());
        }
    }
}
