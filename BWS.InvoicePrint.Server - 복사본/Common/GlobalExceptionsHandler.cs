﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheOne.Diagnostics;

namespace BWS.InvoicePrint.Server.Common
{
    class GlobalExceptionsHandler
    {
        public static void AddHandler()
        {
            System.AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(ConsoleThreadException);
        }

        private static void ConsoleThreadException(object sender, UnhandledExceptionEventArgs e)
        {
            ShowException(e.ExceptionObject as Exception);
        }

        static void ShowException(Exception ex)
        {
            IFoxLog log = FoxLogManager.GetLogger("BWS.InvoicePrint.Server");
            log.Write("GlobalExceptionsHandler : " + ex.Message, FoxLogLevel.Error, ex);
        }
    }
}
