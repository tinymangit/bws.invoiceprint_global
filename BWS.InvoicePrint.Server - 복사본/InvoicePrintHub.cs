﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using TheOne.Diagnostics;

namespace BWS.InvoicePrint.Server
{
    [HubName("invoicePrint")]
    public class InvoicePrintHub : Hub
    {
        #region << 전역변수 >>
        private static IFoxLog logger = FoxLogManager.GetLogger("BWS.InvoicePrint.Server");
        private static List<string> invoicePrintAgents = new List<string>();
        #endregion
        
        public void PrintInvoice(string invoicePrintAgentId, string message)
        {
            var msg = string.Format("InvoicePrintAgentId : {0}, Message : {1}!", invoicePrintAgentId, message);

            Console.WriteLine(msg);

            var all = Clients.All;
            all.printResult(msg);
        }

        //public override Task OnDisconnected(bool stopCalled)
        //{
        //    var connectionId = subscribedClients.FirstOrDefault(x => x.Value == Context.ConnectionId);

        //    if (!string.IsNullOrEmpty(connectionId.Value))
        //    {
        //        subscribedClients.Remove(connectionId.Key);
        //    }
        //    return base.OnDisconnected(true);
        //}
    }
}
