﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TheOne.Diagnostics;
using TheOne.Configuration;
using Microsoft.Owin.Hosting;

namespace BWS.InvoicePrint.Server
{
    public class SignalRServer
    {
        #region << 전역변수 >>
        IFoxLog logger = FoxLogManager.GetLogger("BWS.InvoicePrint.Server");
        private readonly string SERVICE_IP;
        #endregion

        #region << 생성자 >>
        public SignalRServer()
        {
            this.SERVICE_IP = FoxConfigurationManager.AppSettings["SERVICE_IP"];
        }
        #endregion

        #region << Properties >>
        IDisposable SignalR { get; set; }
        #endregion

        #region << public method >>
        public bool StartService()
        {
            logger.Information("Starting service...");

            var option = new StartOptions();
            option.Urls.Add(this.SERVICE_IP);
            SignalR = WebApp.Start<StartUp>(option);

            logger.Information("SignalR server started..");
            logger.Information("Service Started.");
            return true;
        }

        public bool StopService()
        {
            SignalR.Dispose();
            logger.Information("Service Stopped.");
            System.Threading.Thread.Sleep(1500);
            return true;
        }
        #endregion

        #region << private method >>
        #endregion
    }
}
