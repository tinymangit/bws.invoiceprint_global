﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BWS.InvoicePrint.Server
{
    public partial class FrmSignalRTest : Form
    {
        SignalRServer server = new SignalRServer();

        public FrmSignalRTest()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            server.StartService();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            server.StopService();
        }
    }
}
