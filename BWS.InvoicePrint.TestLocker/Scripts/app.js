﻿$(function () {
    var hub = $.connection.hub,
        invoice = $.connection.Invoice,                 // 고정 값
        lockerName = "Locker01",                        // 보관함 이름
        printerAgentName = "PrinterAgent01";            // 송장 출력 할 PrintAgent 명

    // connection error 시 이벤트
    hub.error(function(error) {
        console.log('An error occurred on the hub connection: ' + error);
    });

    // SignalR 서버 접속 URL
    //hub.url = "http://localhost:65077/signalr";
    hub.url = "http://RedPrint-APP-ALB-1529069805.ap-northeast-2.elb.amazonaws.com:65077/signalr";

    // 송장 출력 명령 후 결과 응답 이벤트
    invoice.client.printInvoiceResult = function (resultMsg) {
        console.log(resultMsg);
    }

    // PrintAgent 접속이 끊겼을 때 이벤트
    invoice.client.printAgentDisconnected = function (msg) {
        console.log(msg);
    }

    // signalR 접속 Start
    var started = hub.start();

    // start() 정상 수행 시
    started.done(function () {

        // 최초 1회 SignalR 서버에 Subscribe
        var subscribeLockerMsg = $.parseJSON("{ \"lockerName\": \"" + lockerName + "\", \"printAgentName\" : \"" + printerAgentName + "\" }");
        
        try {

            // 최초 접속 시 보관함 등록 (subscribeLocker 함수 호출)
            var call = invoice.server.subscribeLocker(subscribeLockerMsg);
            call.done(function (resultMsg) {
                console.log(resultMsg);
            });
            // subscribeLocker 함수 호출 에러 시
            call.fail(function (error) {
                console.log('An error has occurred subscribeLocker: ' + error);
            });
        } catch (error) {
            // unhanled error event catch
            console.log('An general SignalR error has occurred somewhere between client and server: ' + error);
        }

        // 송장 출력 이 필요한 이벤트에서 아래 함수 호출
        $('#printInvoice').click(function () {
            var packNum = $('#packNum').val();
            var printInvoiceMsg = $.parseJSON("{ \"lockerName\" : \"" + lockerName + "\", \"companyCode\" : \"0101\", \"languageCode\" : \"LNG_KO\", \"packingNumber\" : \"" + packNum + "\" }");

            // 송장 출력 명령
            invoice.server.printInvoice(printInvoiceMsg);
        });
    });

    // start() 비정상 수행 시
    started.fail(function (error) {
        console.log('An error has occurred connecting: ' + error);
    });
});