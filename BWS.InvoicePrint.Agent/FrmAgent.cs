﻿using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;
using System.Drawing.Printing;
using System.Configuration;
using System.IO;

using ceTe.DynamicPDF.Printing;
using DevExpress.XtraReports.UI;
using InvoicePrint;
using iTextSharp.text.pdf;
using iTextSharp.text;
using Microsoft.AspNet.SignalR.Client;
using Microsoft.Win32;
using TheOne.Diagnostics;
using TheOne.Configuration;

using BWS.InvoicePrint.Core;
using System.Net;
using System.Collections.Generic;
using BWS.InvoicePrint.Agent.Controls;
using System.Drawing;
using DevExpress.XtraEditors;
using BWS.InvoicePrint.Agent.Util;

namespace BWS.InvoicePrint.Agent
{
    public partial class FrmAgent : Form
    {
        IFoxLog log = FoxLogManager.GetLogger("BWS.InvoicePrint.Agent");
        private bool isFormClosing = false;

        private IHubProxy HubProxy { get; set; }

        List<InvoicePalette> PaletteList = new List<InvoicePalette>();
        //private BindingSource bsTarget = new BindingSource();
        DataTable dtYamato = new DataTable();
        DataTable dtYamato2 = new DataTable();

        public FrmAgent()
        {
            InitializeComponent();

            // 창에 버전 정보 표시
            if (string.IsNullOrEmpty(Program.CurrentVersion) == false)
                this.Text += $" ({Program.CurrentVersion})";

            /// 트레이 아이콘 관련 셋팅
            this.MaximizeBox = false;
            this.WindowState = FormWindowState.Normal;
            this.ShowInTaskbar = false;
            //this.Visible = false;
            this.nfIconAgent.Visible = true;
            nfIconAgent.ContextMenuStrip = contextMenuStrip1;
        }

        private void FrmAgent_Load(object sender, EventArgs e)
        {
            this.ServerURI = FoxConfigurationManager.AppSettings["SERVER_URI"];
            this.PrintAgentName = FoxConfigurationManager.AppSettings["PRINT_AGENT_NAME"];
            
            this.btePdfInvPathJ.EditValue = ConfigurationManager.AppSettings["PDF_INV_PATH_J"];
            this.btePdfInvPathY.EditValue = ConfigurationManager.AppSettings["PDF_INV_PATH_Y"];

            try
            {
                string runKey = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Run";
                RegistryKey strUpKey = Registry.LocalMachine.OpenSubKey(runKey);

                if (strUpKey.GetValue("Invoice") == null)
                {
                    strUpKey = Registry.LocalMachine.OpenSubKey(runKey, true);
                    strUpKey.SetValue("Invoice", @"C:\BWS.InvoicePrint\BWS.InvoicePrint.Agent.exe");
                }
            }
            catch
            {
                MessageBox.Show("시작 프로그램 목록에 등록할 수 없습니다. 수동으로 등록 부탁 드립니다.");
            }
        }

        private void FrmAgent_Shown(object sender, EventArgs e)
        {
            this.WriteLog(FoxLogLevel.Information, "++++++++++++++ Get Printer List Start ++++++++++++++ ");

            // 프린터 찾기            
            DataTable dtPrintNameJ = new DataTable("PrintNameJ");            
            dtPrintNameJ.Columns.Add(new DataColumn("PrtNameJ", typeof(string)));
            DataTable dtPrintNameY = new DataTable("PrintNameY");
            dtPrintNameY.Columns.Add(new DataColumn("PrtNameY", typeof(string)));
            PrinterSettings prtSettings = new PrinterSettings();
            foreach (string printer in PrinterSettings.InstalledPrinters)
            {
                DataRow drPrintNameJ = dtPrintNameJ.NewRow();
                drPrintNameJ["PrtNameJ"] = printer;
                dtPrintNameJ.Rows.Add(drPrintNameJ);
                DataRow drPrintNameY = dtPrintNameY.NewRow();
                drPrintNameY["PrtNameY"] = printer;
                dtPrintNameY.Rows.Add(drPrintNameY);
            }
            
            cboPrintNameJ.DataSource = dtPrintNameJ;
            cboPrintNameJ.DisplayMember = "PrtNameJ";
            cboPrintNameJ.ValueMember = "PrtNameJ";
            cboPrintNameJ.BindingContext = this.BindingContext;            
            cboPrintNameJ.SelectedText = ConfigurationManager.AppSettings["PRINT_NAME_J"];
            cboPrintNameJ.SelectedValue = ConfigurationManager.AppSettings["PRINT_NAME_J"];
            cboPrintNameJ.SelectedItem = ConfigurationManager.AppSettings["PRINT_NAME_J"];

            cboPrintNameY.DataSource = dtPrintNameY;
            cboPrintNameY.DisplayMember = "PrtNameY";
            cboPrintNameY.ValueMember = "PrtNameY";
            cboPrintNameY.BindingContext = this.BindingContext;
            cboPrintNameY.SelectedText = ConfigurationManager.AppSettings["PRINT_NAME_Y"];
            cboPrintNameY.SelectedValue = ConfigurationManager.AppSettings["PRINT_NAME_Y"];
            cboPrintNameY.SelectedItem = ConfigurationManager.AppSettings["PRINT_NAME_Y"];

            this.WriteLog(FoxLogLevel.Information, "++++++++++++++ Get Printer List End ++++++++++++++ ");

            this.Connect();
        }

        private async void Connect()
        {
            this.Connection = new HubConnection(this.ServerURI);
            this.Connection.Error += Connection_Error;
            this.Connection.Closed += Connection_Closed;
            this.Connection.Reconnected += Connection_Reconnected;
            this.Connection.TransportConnectTimeout = TimeSpan.FromSeconds(60);

            this.HubProxy = Connection.CreateHubProxy("invoice");

            //Handle incoming event from server: use Invoke to write to console from SignalR's thread
            this.HubProxy.On<PrintInvoiceMessage>("PrintInvoice", (printInvoiceMessage) =>
                this.Invoke((Action)(() =>
                    {
                        PrintInvoice(printInvoiceMessage);
                    }
                ))                
            );
            
            // 네트워크 연결 상태 여부 확인하고 SignalR 연결
            bool availNet = false;
            int retryCnt = 0;
            this.btnConnect.Enabled = false;

            while (availNet == false)
            {
                // 물리적 네트워크 상태 체크
                bool tempNetChk = System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable();

                if (tempNetChk)
                {
                    try
                    {
                        if (retryCnt == 0)
                            this.WriteLog(FoxLogLevel.Information, $"++++++++++++++ 인터넷 연결 확인 중 ++++++++++++++ ");

                        var client = new HttpClient()
                        {
                            Timeout = TimeSpan.FromSeconds(10)
                        };

                        var resMsg = await client.GetAsync(new Uri(ServerURI));

                        if (resMsg.IsSuccessStatusCode)
                        {
                            availNet = true;
                            break;
                        }
                        else
                        {
                            if (retryCnt > 0)
                                this.WriteLog(FoxLogLevel.Information, $"++++++++++++++ [{retryCnt} 회차] : 실패 / 재시도 중... ++++++++++++++ ");

                            // 1초 후 재시도
                            await Task.Delay(1000);
                        }
                    }
                    catch
                    {

                    }
                }
                else
                {
                    this.btnConnect.Enabled = true;

                    this.WriteLog(FoxLogLevel.Information, "++++++++++++++ 네트워크 연결 상태를 확인 후 '연결' 버튼을 클릭해주세요. [선 연결 상태 및 인터넷 연결 가능 여부] ++++++++++++++ ");
                    return;
                }

                retryCnt++;

                // 일정 시도 이후 실패 표시하고 사용자에게 접속 시도를 요구
                if (retryCnt > 10)
                {
                    this.btnConnect.Enabled = true;

                    this.WriteLog(FoxLogLevel.Information, "++++++++++++++ 네트워크 연결 상태를 확인 후 '연결' 버튼을 클릭해주세요. [선 연결 상태 및 인터넷 연결 가능 여부] ++++++++++++++ ");
                    return;
                }
            }

            try
            {
                this.WriteLog(FoxLogLevel.Information, "++++++++++++++ Connecting Server ++++++++++++++ ");

                this.Connection.Start().Wait();

                if (this.Connection.State == Microsoft.AspNet.SignalR.Client.ConnectionState.Connected)
                {
                    this.WriteLog(FoxLogLevel.Information, "++++++++++++++ Connected !! ++++++++++++++ ");

                    // 버튼 비활성화
                    this.btnConnect.Enabled = false;

                    var response = await this.SubscribeAgent();
                    
                    this.WriteLog(FoxLogLevel.Information, "SubscribeAgent Called !! - {0}", response.ToString());
                    
                }
            }
            catch (HttpRequestException ex)
            {
                string msg = "Unable to connect to server: Start server before connecting clients.\r\n{0}";
                this.WriteLog(FoxLogLevel.Error, msg, ex.ToString());

                return;
            }
            catch (Exception ex)
            {
                this.WriteLog(FoxLogLevel.Error, ex.ToString());
                return;
            }
        }

        private async Task<SubscribeAgentMessage> SubscribeAgent()
        {
            var message = new SubscribeAgentMessage() { PrintAgentName = this.PrintAgentName };

            return await this.HubProxy.Invoke<SubscribeAgentMessage>("SubscribeAgent", message);
        }

        /// <summary>   
        /// 서버에서 인쇄 명령이 오게 되면 호출 되는 함수
        /// </summary>
        /// <param name="printJsonMessage"></param>
        private void PrintInvoice(PrintInvoiceMessage printInvoiceMessage)
        {
            this.WriteLog(FoxLogLevel.Information, "PrintInvoice Called - {0}", printInvoiceMessage.ToString());
            this.txtLog.Text += "\n" + printInvoiceMessage.ToString();

            // 출력 전 서버로부터 요청을 제대로 받았다는 상태 저장
            // 이 기능으로 인해 예외가 발생해서 프로그램 동작에 이슈 없도록 구현
            string etcMsg = string.Empty;
            try
            {
                this.HubProxy.Invoke("PrintInvoiceSaveLog", printInvoiceMessage, (int)Core.PrintState.PRT_AGENT, etcMsg);
            }
            catch (Exception ex)
            {
                log.Warning($"[출력 준비에 대한 로그 기록 에러] {ex.ToString()}");
            }

            #region << 인쇄 처리 >>

            // 기본 프린터 찾기
            string printerName = string.Empty;

            PrinterSettings settings = new PrinterSettings();

            foreach (string printer in PrinterSettings.InstalledPrinters)
            {
                settings.PrinterName = printer;

                if (ConfigurationManager.AppSettings["PRINT_NAME"] != null)
                {
                    printerName = ConfigurationManager.AppSettings["PRINT_NAME"];
                    break;
                }
                else if (ConfigurationManager.AppSettings["PRINT_NAME_Y"] != null)
                {
                    printerName = ConfigurationManager.AppSettings["PRINT_NAME_Y"];
                    break;
                }
                else
                {
                    if (settings.IsDefaultPrinter)
                    {
                        printerName = printer;
                        break;
                    }
                }
            }

            try
            {
                if (string.IsNullOrEmpty(printerName))
                {
                    throw new Exception("기본 프린터가 설정되어 있지 않습니다.");
                }

                DataTable dtOrdNatGb = printInvoiceMessage.InvoiceOrdNatGbList;

                //if (printInvoiceMessage.InvType == "GSAGAWA")
                if (dtOrdNatGb.Rows[0]["DLVR_MTD_GB"].ToString() == "DLV_P41" || dtOrdNatGb.Rows[0]["DLVR_MTD_GB"].ToString() == "DLV_P42") // 사가와
                {
                    printerName = ConfigurationManager.AppSettings["PRINT_NAME_J"];

                    var cellNo = dtOrdNatGb.Rows[0]["LOCKER_NUM"].ToString();
                    var dlvrReq = dtOrdNatGb.Rows[0]["DLVR_REQ"].ToString();
                    string packFileName = string.Format("{0}.pdf", dtOrdNatGb.Rows[0]["INV_NUM"].ToString()); // 송장번호
                    
                    string packSrcFilePath = Path.Combine(this.btePdfInvPathJ.EditValue.ToString(), packFileName);
                    string packTempFilePath = Path.Combine(Path.GetTempPath(), packFileName);
                    
                    using (PdfReader reader = new PdfReader(packSrcFilePath))

                    using (PdfStamper stamper = new PdfStamper(reader, new FileStream(packTempFilePath, FileMode.Create)))
                    {
                        for (int i = 1; i <= reader.NumberOfPages; i++)
                        {
                            var pdfSize = reader.GetPageSize(i);

                            PdfContentByte cb = stamper.GetOverContent(i);

                            iTextSharp.text.Font font = new iTextSharp.text.Font();
                            font.Size = 15;

                            ColumnText.ShowTextAligned(cb, Element.ALIGN_CENTER, new Phrase(string.Format("{0}", cellNo), font), pdfSize.Width - 130, pdfSize.Height - 150, 90); //x좌표 - 작아지는게 오른쪽으로, y좌표 - 작아지는게 아래쪽으로

                            var localfontPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), @"Microsoft\Windows\Fonts");

                            var fontPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts), "NotoSansCJKjp-Regular.otf");

                            if (Directory.Exists(localfontPath) == true)
                            {
                                fontPath = Path.Combine(localfontPath, "NotoSansCJKjp-Regular.otf");
                            }    
                                                    
                            BaseFont bf = BaseFont.CreateFont(fontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                            
                            iTextSharp.text.Font fontReq = new iTextSharp.text.Font(bf);
                            fontReq.Size = 6;

                            ColumnText.ShowTextAligned(cb, Element.ALIGN_BOTTOM, new Phrase(string.Format("{0}", dlvrReq), fontReq), pdfSize.Width - 121, pdfSize.Height - 263, 90); //x좌표 - 작아지는게 오른쪽으로, y좌표 - 작아지는게 아래쪽으로                                                        
                        }
                    }
                    
                    byte[] pdfBytes = FileToByteArray(packTempFilePath); // PDF 저장경로 + 파일명

                    PrintJob printJob = new PrintJob(printerName);
                    // 파일명 지정 - 스풀리스트 이름
                    printJob.DocumentName = string.Format("{0}", packFileName);
                    // 인쇄매수 지정
                    printJob.PrintOptions.Copies = 1;

                    InputPdf inputPdf = new InputPdf(pdfBytes);

                    // 페이지
                    printJob.Pages.Add(inputPdf);

                    printJob.Starting += printJob_Starting;
                    printJob.Succeeded += printJob_Succeeded;
                    printJob.Failed += printJob_Failed;
                    printJob.Updated += printJob_Updated;
                    printJob.Print();

                    inputPdf.Dispose();
                }
                //else if (dtOrdNatGb.Rows[0]["DLVR_MTD_GB"].ToString() == "DLV_Y01") // 야마토
                else if (printInvoiceMessage.InvType == "YAMATO") // 야마토
                {
                    printerName = ConfigurationManager.AppSettings["PRINT_NAME_Y"];

                    dtYamato = dtOrdNatGb.Copy();
                    dtYamato2 = dtOrdNatGb.Copy();
                    dtYamato2.Rows.Clear();

                    this.WriteLog(FoxLogLevel.Information, dtYamato.Rows[0]["INV_NUM"].ToString());

                    if (dtOrdNatGb.Rows[0]["PRINT_YN"].ToString().Equals("Y") && XtraMessageBox.Show($"출력된 기록[{dtOrdNatGb.Rows[0]["PRINT_DT"].ToString()}]이 있습니다. 출력하시겠습니까?", "확인", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        return;

                    for (int i = 0; i < dtOrdNatGb.Rows.Count; i++)
                    {
                        this.SendYamatoInvoice(dtOrdNatGb.Rows[i]);
                    }

                    dtYamato = dtYamato2;
                    this.InvoicePrint();
                }
            }
            catch (Exception ex)
            {
                printInvoiceMessage.IsSuccess = false;
                printInvoiceMessage.ErrorMessage = ex.Message;
                etcMsg = ex.ToString();
            }
            finally
            {
                try
                {
                    // 결과 반환 전 로그 저장
                    // 이 기능으로 인해 예외가 발생해서 프로그램 동작에 이슈 없도록 구현
                    this.HubProxy.Invoke("PrintInvoiceSaveLog", printInvoiceMessage, (int)Core.PrintState.COMP_TO_SERV, etcMsg);
                }
                catch (Exception ex)
                {
                    log.Warning($"[결과 완료 로그 기록 중 에러] {ex.ToString()}");
                }

                this.HubProxy.Invoke("PrintInvoiceResult", printInvoiceMessage);
            }
            #endregion
        }

        private void WriteLog(FoxLogLevel logLevel, string message, params string[] strings)
        {
            if (this.InvokeRequired)
            {
                Invoke(new MethodInvoker(delegate ()
                {
                    WriteLog(logLevel, message, strings);
                }));
            }
            else
            {
                string msg = string.Format(message, strings);

                // 1000 라인 넘어가면 초기화
                if (this.txtLog.Lines.Count() > 1000)
                    this.txtLog.EditValue = string.Empty;

                this.txtLog.EditValue += msg + "\r\n";
                this.txtLog.SelectionStart = this.txtLog.Text.Length;
                this.txtLog.ScrollToCaret();

                log.Write(logLevel, msg);
            }
        }

        private void SendYamatoInvoice(DataRow row)
        {
            var invNum = row["INV_NUM"].ToString();
            var ordNum = row["ORD_NUM"].ToString();
            string invPeacePath = this.btePdfInvPathY.EditValue.ToString();

            var filePath = Path.Combine(invPeacePath, $"{invNum}.pdf");
            if (File.Exists(filePath) == false)
            {
                XtraMessageBox.Show($"{filePath} 파일을 찾을 수 없습니다.");
                return;
            }
            
            //if (dt.Select($"ORD_NUM = '{row["ORD_NUM"].ToString()}'").Count() > 0)
            //    return;

            var newRow = dtYamato2.NewRow();
            newRow["ORD_NUM"] = row["ORD_NUM"];
            newRow["INV_NUM"] = row["INV_NUM"];
            newRow["LOCKER_NUM"] = row["LOCKER_NUM"];
            newRow["DLVR_REQ"] = row["DLVR_REQ"];
            newRow["DLVR_MTD_GB"] = row["DLVR_MTD_GB"];
            newRow["PRINT_YN"] = row["PRINT_YN"];
            newRow["PRINT_DT"] = row["PRINT_DT"];
            newRow["FILE_PATH"] = filePath;
            dtYamato2.Rows.Add(newRow);

            AddInovicePalette();

            //if (this.PaletteList.LastOrDefault() == null || this.PaletteList.Last().IsEmpty == false)
            //{
            //    AddInovicePalette();
            //}
            //else
            //{
            //    if (this.PaletteList.Last().IsEmpty == false)
            //    {
            //        AddInovicePalette();
            //    }
            //}

            this.PaletteList.Last().Push(ordNum);

            //if (this.PaletteList.Last().IsEmpty == false)
            //    this.InvoicePrint();
        }

        private void AddInovicePalette()
        {
            var item = new InvoicePalette() { Dock = DockStyle.Top };

            if (this.PaletteList.Count % 2 == 1)
                item.BackColor = Color.LightGray;

            //this.xtraScrollableControl1.Controls.Add(item);
            this.PaletteList.Add(item);
        }

        private void ClearInvoicePalette()
        {
            //(this.bsTarget.DataSource as DataTable).Rows.Clear();
            //this.xtraScrollableControl1.Controls.Clear();
            dtYamato.Rows.Clear();
            this.PaletteList.Clear();

            this.AddInovicePalette();
        }

        private void InvoicePrint()
        {
            //파일병합
            var dt = dtYamato;
            if (dt.Rows.Count == 0)
            {
                XtraMessageBox.Show($"출력 할 데이터가 없습니다.");
                return;
            }

            try
            {
                //var filePath = Path.Combine(Path.GetTempPath(), $"{Guid.NewGuid().ToString()}.pdf");
                var filePath = Path.Combine(Path.GetTempPath(), $"{DateTime.Now.ToString("yyyyMMdd")}_YAMATO.pdf");
                PDFlibHelper.InvoiceDataToPrintData(filePath, dt);

                //출력
                var printerName = ConfigurationManager.AppSettings["PRINT_NAME_Y"];
                
                byte[] pdfBytes = FileToByteArray(filePath); // PDF 저장경로 + 파일명

                PrintJob printJob = new PrintJob(printerName);
                // 파일명 지정 - 스풀리스트 이름
                printJob.DocumentName = string.Format("{0}", $"{DateTime.Now.ToString("yyyyMMdd")}_YAMATO.pdf");
                // 인쇄매수 지정
                printJob.PrintOptions.Copies = 1;

                InputPdf inputPdf = new InputPdf(pdfBytes);

                // 페이지
                printJob.Pages.Add(inputPdf);

                printJob.Starting += printJob_Starting;
                printJob.Succeeded += printJob_Succeeded;
                printJob.Failed += printJob_Failed;
                printJob.Updated += printJob_Updated;
                printJob.Print();

                inputPdf.Dispose();

                //클리어
                this.ClearInvoicePalette();
                File.Delete(filePath);
            }
            finally
            {
                
            }
        }

        private void Connection_Error(Exception ex)
        {
            string msg = "Connection_Error!!\r\n{0}";
            this.WriteLog(FoxLogLevel.Error, msg, ex.ToString());

            // 폼 표시
            this.Visible = true;
            if (this.WindowState == FormWindowState.Minimized)
                this.WindowState = FormWindowState.Normal;

            // 폼 활성화
            this.Activate();

            // 버튼 활성화
            this.btnConnect.Enabled = true;
        }
        /// <summary>
        /// DisconnectTimeout 설정 시간 만큼 계속 연결을 시도 하다 시간이 초과하면 Connection_Closed 이벤트가 발생한다.
        /// </summary>
        private void Connection_Closed()
        {
            this.WriteLog(FoxLogLevel.Information, "++++++++++++++ Connection Closed!! ++++++++++++++ ");

            // 버튼 활성화
            this.btnConnect.Enabled = true;

            // 이벤트 발생 후에도 계속 시도 하려면 다음 아래 부분을 활성화
            // 그냥 종료 시키려면 주석 처리
            if (!this.isFormClosing)
            {
                this.WriteLog(FoxLogLevel.Information, "Connect 재시도..");
                this.Connect();

                //Task.Run(() =>
                //{
                //    this.WriteLog(FoxLogLevel.Information, "Connect 재시도..");
                //    this.Connect();
                //});
            }
        }

        private async void Connection_Reconnected()
        {
            this.WriteLog(FoxLogLevel.Information, "++++++++++++++ Connection Reconnected!! ++++++++++++++ ");

            var response = await this.SubscribeAgent();

            // 폼 표시
            this.Visible = true;
            if (this.WindowState == FormWindowState.Minimized)
                this.WindowState = FormWindowState.Normal;

            // 폼 활성화
            this.Activate();

            // 버튼 비활성화
            this.btnConnect.Enabled = false;

            this.WriteLog(FoxLogLevel.Information, "SubscribeAgent Called !! - {0}", response.ToString());
        }

        private void FrmAgent_FormClosing(object sender, FormClosingEventArgs e)
        {
            //// 트레이아이콘 Visible 처리
            //this.WindowState = FormWindowState.Minimized;
            if (e.CloseReason == CloseReason.ApplicationExitCall)
            {
                this.isFormClosing = true;

                if (this.Connection != null && this.isFormClosing)
                {
                    this.Connection.Stop();
                    this.Connection.Dispose();
                }
            }
            else
            {
                e.Cancel = true;
                this.Hide();
                this.nfIconAgent.Visible = true;
            }
        }

        private string ServerURI { get; set; }

        private string PrintAgentName { get; set; }

        private HubConnection Connection { get; set; }

        // 글로벌 관련 송장 PDF 저장되어 있는 경로
        private string PdfInvFilePath { get; set; }

        /// <summary>
        /// 연결하기 버튼 Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnConnect_Click(object sender, EventArgs e)
        {
            this.Connect();
        }

        /// <summary>
        /// 로그 저장 테스트 (임시 버튼 만들어 이벤트 추가해서 테스트)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnLogTest_Click(object sender, EventArgs e)
        {
            var printInvoiceMessage = new PrintInvoiceMessage()
            {
                ComapnyCode = "0101",
                LanguageCode = "LNG_KO",
                PackingNumber = "1509210002",
            };

            this.HubProxy.Invoke("PrintInvoiceSaveLog", printInvoiceMessage, (int)Core.PrintState.COMP_TO_SERV, "업데이트 테스트");
        }

        /// <summary>
        /// 트레이 아이콘 Double Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void nfIconAgent_DoubleClick(object sender, EventArgs e)
        {
            // 폼 표시
            this.Visible = true;
            if (this.WindowState == FormWindowState.Minimized)
                this.WindowState = FormWindowState.Normal;

            // 폼 활성화
            this.Activate();
        }

        /// <summary>
        /// 트레이 아이콘 종료 MenuItem Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // 트레이아이콘 Visible 처리
            //this.nfIconAgent.Visible = false;            
            nfIconAgent.Dispose();
            // 프로세스 종료            
            Application.Exit();
        }
        
        /// <summary>
        /// 사가와 PDF 송장 가져오는 경로
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btePdfInvPathJ_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            try
            {
                var fbd = new FolderBrowserDialog();
                fbd.SelectedPath = this.PdfInvFilePath;
                fbd.ShowNewFolderButton = false;
                fbd.ShowDialog();
                this.btePdfInvPathJ.EditValue = fbd.SelectedPath;
                //if (fbd.ShowDialog())
                //{
                //    this.btePdfInvPath.EditValue = fbd.SelectedPath;
                //}
            }
            catch (Exception ex)
            {
                this.WriteLog(FoxLogLevel.Error, ex.ToString());
                return;
            }
        }

        /// <summary>
        /// 야마토 PDF 송장 가져오는 경로
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btePdfInvPathY_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            try
            {
                var fbd = new FolderBrowserDialog();
                fbd.SelectedPath = this.PdfInvFilePath;
                fbd.ShowNewFolderButton = false;
                fbd.ShowDialog();
                this.btePdfInvPathY.EditValue = fbd.SelectedPath;                
            }
            catch (Exception ex)
            {
                this.WriteLog(FoxLogLevel.Error, ex.ToString());
                return;
            }
        }

        /// <summary>
        /// 사가와 송장 프린터 설정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSaveJ_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.btePdfInvPathJ.EditValue.ToString()))
            {
                this.btePdfInvPathJ.Focus();
            }
            else
            {
                DataRow drPrtName = ((DataRowView)cboPrintNameJ.SelectedItem).Row;
                System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                if (config.AppSettings.Settings["PRINT_NAME_J"] == null && config.AppSettings.Settings["PDF_INV_PATH_J"] == null)
                {
                    config.AppSettings.Settings.Add("PRINT_NAME_J", drPrtName["PrtNameJ"].ToString());
                    config.AppSettings.Settings.Add("PDF_INV_PATH_J", this.btePdfInvPathJ.EditValue.ToString());
                }
                else if (config.AppSettings.Settings["PRINT_NAME_J"] != null && config.AppSettings.Settings["PDF_INV_PATH_J"] == null)
                {
                    config.AppSettings.Settings["PRINT_NAME_J"].Value = drPrtName["PrtNameJ"].ToString();
                    config.AppSettings.Settings.Add("PDF_INV_PATH_J", this.btePdfInvPathJ.EditValue.ToString());
                }
                else
                {
                    config.AppSettings.Settings["PRINT_NAME_J"].Value = drPrtName["PrtNameJ"].ToString();
                    config.AppSettings.Settings["PDF_INV_PATH_J"].Value = this.btePdfInvPathJ.EditValue.ToString();
                }

                config.Save(ConfigurationSaveMode.Modified, true);
                ConfigurationManager.RefreshSection("appSettings");

                MessageBox.Show("저장되었습니다.");
            }
        }

        /// <summary>
        /// 야마토 송장 프린터 설정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSaveY_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.btePdfInvPathY.EditValue.ToString()))
            {
                this.btePdfInvPathY.Focus();
            }
            else
            {
                DataRow drPrtName = ((DataRowView)cboPrintNameY.SelectedItem).Row;
                System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                if (config.AppSettings.Settings["PRINT_NAME_Y"] == null && config.AppSettings.Settings["PDF_INV_PATH_Y"] == null)
                {
                    config.AppSettings.Settings.Add("PRINT_NAME_Y", drPrtName["PrtNameY"].ToString());
                    config.AppSettings.Settings.Add("PDF_INV_PATH_Y", this.btePdfInvPathJ.EditValue.ToString());
                }
                else if (config.AppSettings.Settings["PRINT_NAME_Y"] != null && config.AppSettings.Settings["PDF_INV_PATH_Y"] == null)
                {
                    config.AppSettings.Settings["PRINT_NAME_Y"].Value = drPrtName["PrtNameY"].ToString();
                    config.AppSettings.Settings.Add("PDF_INV_PATH_Y", this.btePdfInvPathY.EditValue.ToString());
                }
                else
                {
                    config.AppSettings.Settings["PRINT_NAME_Y"].Value = drPrtName["PrtNameY"].ToString();
                    config.AppSettings.Settings["PDF_INV_PATH_Y"].Value = this.btePdfInvPathY.EditValue.ToString();
                }

                config.Save(ConfigurationSaveMode.Modified, true);
                ConfigurationManager.RefreshSection("appSettings");

                MessageBox.Show("저장되었습니다.");
            }
        }

        #region <<DynamicPDF 관련 이벤트>>
        /// <summary>
        /// 출력 수정
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void printJob_Updated(object sender, PrintJobEventArgs e)
        {
            //this.WritePrinterLog(string.Format("Update Log.. DocumentName - {0},Page Count - {1},Printed Page - {2},Printer Name - {3},Status - {4}", e.PrintJob.DocumentName, e.PrintJob.Pages.Count.ToString(), e.PrintJob.PagesPrinted.ToString(), e.PrintJob.Printer.Name, e.PrintJob.Status.ToString()));
            //this.WritePrinterLog(string.Format("JobId - {0}, Printer.Name - {1}, Status - {2}, Page Count - {3},Printed Page - {4}", e.PrintJob.JobId.ToString(), e.PrintJob.Printer.Name, e.PrintJob.Status.ToString(), e.PrintJob.Pages.Count.ToString(), e.PrintJob.PagesPrinted.ToString()));
        }

        /// <summary>
        /// 출력 실패
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void printJob_Failed(object sender, PrintJobFailedEventArgs e)
        {
            // 파일명 예) [201708281356088973]_20170816_1605051621_2.pdf - [1Copy]
            // 파일명 예) [201708281356088973]_20170816_1605051621_2.pdf
            string pdfFilePath = Path.GetDirectoryName(e.PrintJob.DocumentName);
            string pdfFileName = Path.GetFileName(e.PrintJob.DocumentName);
            pdfFileName = pdfFileName.Substring(0, pdfFileName.IndexOf(".pdf")) + ".pdf";
            pdfFilePath = Path.Combine(pdfFilePath, pdfFileName);

            // 출력 실패 메시지
            MessageBox.Show(string.Format("{0} 경로의 {1} 파일 확인!", pdfFilePath, pdfFileName));

            //this.WritePrinterLog("Print Failed.. Printer Name  - {0}, File Name - {1}, Page - {2}, Copy - {3}", e.PrintJob.Printer.Name, pdfFilePath, e.PrintJob.Pages.Count.ToString(), e.PrintJob.PrintOptions.Copies.ToString());
            //this.WriteNLog(pdfFilePath, string.Format("Failed.. 프린터명 - {0}, 페이지수 - {1}, Copy - {2}", e.PrintJob.Printer.Name, e.PrintJob.Pages.Count, e.PrintJob.PrintOptions.Copies), 200);

            //string data = PdfUtil.GetMetadataInPdf(pdfFilePath, "Info");
            //PrintJobInfo printJobInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<PrintJobInfo>(data);

            //// 인쇄 실패
            //this.FailedPrinting(printJobInfo);
        }

        /// <summary>
        /// 출력 성공
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void printJob_Succeeded(object sender, PrintJobEventArgs e)
        {
            // 파일명 예) [201708281356088973]_20170816_1605051621_2.pdf - [1Copy]
            // 파일명 예) [201708281356088973]_20170816_1605051621_2.pdf
            string pdfFilePath = Path.GetDirectoryName(e.PrintJob.DocumentName);
            string pdfFileName = Path.GetFileName(e.PrintJob.DocumentName);
            pdfFileName = pdfFileName.Substring(0, pdfFileName.IndexOf(".pdf")) + ".pdf";
            pdfFilePath = Path.Combine(pdfFilePath, pdfFileName);

            // 임시폴더의 PDF 삭제
            File.Delete(pdfFilePath);
        }

        /// <summary>
        /// 출력 시작
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void printJob_Starting(object sender, PrintJobStartingEventArgs e)
        {
            //// 파일명 예) [201708281356088973]_20170816_1605051621_2.pdf - [1Copy]
            //// 파일명 예) [201708281356088973]_20170816_1605051621_2.pdf
            //string pdfFilePath = Path.GetDirectoryName(e.PrintJob.DocumentName);
            //string pdfFileName = Path.GetFileName(e.PrintJob.DocumentName);
            //pdfFileName = pdfFileName.Substring(0, pdfFileName.IndexOf(".pdf")) + ".pdf";
            //pdfFilePath = Path.Combine(pdfFilePath, pdfFileName);

            //this.WritePrinterLog("Print Starting.. Printer Name  - {0}, File Name - {1}, Page - {2}, Copy - {3}", e.PrintJob.Printer.Name, pdfFilePath, e.PrintJob.Pages.Count.ToString(), e.PrintJob.PrintOptions.Copies.ToString());
            //this.WriteNLog(pdfFilePath, string.Format("Starting.. 프린터명 - {0}, 페이지수 - {1}, Copy - {2}", e.PrintJob.Printer.Name, e.PrintJob.Pages.Count, e.PrintJob.PrintOptions.Copies), 100);
        }

        /// <summary>
        /// PDF 파일을 Array로 변경하는 Method
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static byte[] FileToByteArray(string fileName)
        {
            byte[] buff = null;
            using (FileStream fs = new FileStream(fileName,
                                           FileMode.Open,
                                           FileAccess.Read, FileShare.Read))
            {
                BinaryReader br = new BinaryReader(fs);
                long numBytes = new FileInfo(fileName).Length;
                buff = br.ReadBytes((int)numBytes);
            }

            return buff;
        }
        #endregion        
    }
}
