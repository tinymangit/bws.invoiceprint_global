﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BWS.InvoicePrint.Agent
{
    public partial class FrmCheckForUpdate : Form
    {
        public FrmCheckForUpdate()
        {
            InitializeComponent();
        }

        private void FrmCheckForUpdate_Load(object sender, EventArgs e)
        {
            this.automaticUpdater1.Cancelled += runMainApplication;
            this.automaticUpdater1.CheckingFailed += runMainApplication;
            this.automaticUpdater1.ClosingAborted += runMainApplication;
            this.automaticUpdater1.DownloadingOrExtractingFailed += runMainApplication;
            this.automaticUpdater1.UpdateFailed += runMainApplication;
            this.automaticUpdater1.UpToDate += runMainApplication;

            this.automaticUpdater1.ReadyToBeInstalled += automaticUpdater_ReadyToBeInstalled;
        }

        private void automaticUpdater_ReadyToBeInstalled(object sender, EventArgs e)
        {
            this.automaticUpdater1.InstallNow();

            AppVersionModify();
        }


        private void runMainApplication(object sender, EventArgs e)
        {
            this.Visible = false;

            var frm = new FrmAgent();
            frm.FormClosed += frm_FormClosed;
            frm.Show();
        }


        private void frm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close();
        }


        private void FrmCheckForUpdate_Shown(object sender, EventArgs e)
        {
            this.automaticUpdater1.ForceCheckForUpdate(true);

            AppVersionModify();
        }

        private void AppVersionModify()
        {
            string curVer = Program.CurrentVersion;

            if(string.IsNullOrEmpty(curVer))
                Program.CurrentVersion = this.automaticUpdater1.Version;
            else if(curVer != this.automaticUpdater1.Version)
                Program.CurrentVersion = this.automaticUpdater1.Version;
        }
    }
}
