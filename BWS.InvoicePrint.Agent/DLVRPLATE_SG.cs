﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace BWS.InvoicePrint.Agent
{
    public partial class DLVRPLATE_SG : DevExpress.XtraReports.UI.XtraReport
    {
        public DLVRPLATE_SG(DataSet ds)
        {
            InitializeComponent();

            this.DataSource = ds.Tables[1];

            xrBarCode1.DataBindings.Add("Text", ds.Tables[0], "INV_NUM"); // 송장 바코드
            xrLabel11.DataBindings.Add("Text", ds.Tables[0], "CNT"); // 주문 건수
            xrLabel12.DataBindings.Add("Text", ds.Tables[0], "INV_NUM"); // 송장 Text
            xrLabel10.DataBindings.Add("Text", ds.Tables[0], "NM"); // Consignee

            colNo.DataBindings.Add("Text", ds.Tables[1], "ROW_NUM");
            colOrdNum.DataBindings.Add("Text", ds.Tables[1], "ORDER_NUM");
            colOrdCnt.DataBindings.Add("Text", ds.Tables[1], "PRN_CNT");
            colOrdCnts.DataBindings.Add("Text", ds.Tables[1], "PDT_DESCRIPTION");
        }

    }
}
