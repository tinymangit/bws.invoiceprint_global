﻿namespace BWS.InvoicePrint.Agent
{
    partial class FrmAgent
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAgent));
            this.txtLog = new DevExpress.XtraEditors.MemoEdit();
            this.btnConnect = new System.Windows.Forms.Button();
            this.nfIconAgent = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ExitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cboPrintNameJ = new System.Windows.Forms.ComboBox();
            this.btePdfInvPathJ = new DevExpress.XtraEditors.ButtonEdit();
            this.btnSaveJ = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cboPrintNameY = new System.Windows.Forms.ComboBox();
            this.btePdfInvPathY = new DevExpress.XtraEditors.ButtonEdit();
            this.btnSaveY = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.txtLog.Properties)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btePdfInvPathJ.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btePdfInvPathY.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // txtLog
            // 
            this.txtLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLog.Location = new System.Drawing.Point(12, 97);
            this.txtLog.Name = "txtLog";
            this.txtLog.Size = new System.Drawing.Size(850, 408);
            this.txtLog.TabIndex = 0;
            // 
            // btnConnect
            // 
            this.btnConnect.Enabled = false;
            this.btnConnect.Location = new System.Drawing.Point(12, 12);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(125, 79);
            this.btnConnect.TabIndex = 1;
            this.btnConnect.Text = "연결하기";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // nfIconAgent
            // 
            this.nfIconAgent.Icon = ((System.Drawing.Icon)(resources.GetObject("nfIconAgent.Icon")));
            this.nfIconAgent.Text = "InvoicePrintAgent";
            this.nfIconAgent.Visible = true;
            this.nfIconAgent.DoubleClick += new System.EventHandler(this.nfIconAgent_DoubleClick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ExitToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(99, 26);
            // 
            // ExitToolStripMenuItem
            // 
            this.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem";
            this.ExitToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
            this.ExitToolStripMenuItem.Text = "종료";
            this.ExitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // cboPrintNameJ
            // 
            this.cboPrintNameJ.FormattingEnabled = true;
            this.cboPrintNameJ.Location = new System.Drawing.Point(188, 9);
            this.cboPrintNameJ.Name = "cboPrintNameJ";
            this.cboPrintNameJ.Size = new System.Drawing.Size(186, 20);
            this.cboPrintNameJ.TabIndex = 5;
            // 
            // btePdfInvPathJ
            // 
            this.btePdfInvPathJ.Location = new System.Drawing.Point(378, 8);
            this.btePdfInvPathJ.Name = "btePdfInvPathJ";
            this.btePdfInvPathJ.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.btePdfInvPathJ.Size = new System.Drawing.Size(289, 20);
            this.btePdfInvPathJ.TabIndex = 6;
            this.btePdfInvPathJ.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btePdfInvPathJ_ButtonClick);
            // 
            // btnSaveJ
            // 
            this.btnSaveJ.Location = new System.Drawing.Point(673, 6);
            this.btnSaveJ.Name = "btnSaveJ";
            this.btnSaveJ.Size = new System.Drawing.Size(49, 23);
            this.btnSaveJ.TabIndex = 7;
            this.btnSaveJ.Text = "저장";
            this.btnSaveJ.UseVisualStyleBackColor = true;
            this.btnSaveJ.Click += new System.EventHandler(this.btnSaveJ_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(143, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 12;
            this.label2.Text = "사가와";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(145, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 13;
            this.label1.Text = "야마토";
            // 
            // cboPrintNameY
            // 
            this.cboPrintNameY.FormattingEnabled = true;
            this.cboPrintNameY.Location = new System.Drawing.Point(188, 36);
            this.cboPrintNameY.Name = "cboPrintNameY";
            this.cboPrintNameY.Size = new System.Drawing.Size(186, 20);
            this.cboPrintNameY.TabIndex = 14;
            // 
            // btePdfInvPathY
            // 
            this.btePdfInvPathY.Location = new System.Drawing.Point(378, 34);
            this.btePdfInvPathY.Name = "btePdfInvPathY";
            this.btePdfInvPathY.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.btePdfInvPathY.Size = new System.Drawing.Size(289, 20);
            this.btePdfInvPathY.TabIndex = 15;
            this.btePdfInvPathY.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btePdfInvPathY_ButtonClick);
            // 
            // btnSaveY
            // 
            this.btnSaveY.Location = new System.Drawing.Point(673, 29);
            this.btnSaveY.Name = "btnSaveY";
            this.btnSaveY.Size = new System.Drawing.Size(49, 23);
            this.btnSaveY.TabIndex = 16;
            this.btnSaveY.Text = "저장";
            this.btnSaveY.UseVisualStyleBackColor = true;
            this.btnSaveY.Click += new System.EventHandler(this.btnSaveY_Click);
            // 
            // FrmAgent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 517);
            this.Controls.Add(this.btnSaveY);
            this.Controls.Add(this.btePdfInvPathY);
            this.Controls.Add(this.cboPrintNameY);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnSaveJ);
            this.Controls.Add(this.btePdfInvPathJ);
            this.Controls.Add(this.cboPrintNameJ);
            this.Controls.Add(this.btnConnect);
            this.Controls.Add(this.txtLog);
            this.Name = "FrmAgent";
            this.Text = "InvoicePrintAgent_Global";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmAgent_FormClosing);
            this.Load += new System.EventHandler(this.FrmAgent_Load);
            this.Shown += new System.EventHandler(this.FrmAgent_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.txtLog.Properties)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btePdfInvPathJ.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btePdfInvPathY.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.MemoEdit txtLog;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.NotifyIcon nfIconAgent;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ExitToolStripMenuItem;
        private System.Windows.Forms.ComboBox cboPrintNameJ;
        private DevExpress.XtraEditors.ButtonEdit btePdfInvPathJ;
        private System.Windows.Forms.Button btnSaveJ;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboPrintNameY;
        private DevExpress.XtraEditors.ButtonEdit btePdfInvPathY;
        private System.Windows.Forms.Button btnSaveY;
    }
}

