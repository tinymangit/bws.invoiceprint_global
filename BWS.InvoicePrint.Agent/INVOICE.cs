﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace InvoicePrint
{
    public partial class INVOICE : DevExpress.XtraReports.UI.XtraReport
    {
        public INVOICE(DataTable dt)
        {
            InitializeComponent();
            
            xrBarCode4.DataBindings.Add("Text", dt, "POST_URL");
            //xrBarCode4.DataBindings.Add("NavigateUrl", ds.Tables[0], "POST_URL");
            
            xrLabel17.DataBindings.Add("Text", dt, "ACPT_POST_NM");
            xrLabel18.DataBindings.Add("Text", dt, "INV_REQ_YMD");

            xrLabel9.DataBindings.Add("Text", dt, "SNDR_NM");

            xrLabel19.DataBindings.Add("Text", dt, "PACKING_NUM");
            xrLabel6.DataBindings.Add("Text", dt, "WGT");
            xrLabel21.DataBindings.Add("Text", dt, "SIZE");
            xrLabel23.DataBindings.Add("Text", dt, "DLVR_AMT");

            xrBarCode1.DataBindings.Add("Text", dt, "ZIP");

            xrLabel25.DataBindings.Add("Text", dt, "PDT_LIST");
            
            xrLabel33.DataBindings.Add("Text", dt, "ARR_CEN_POST_CD");
            xrLabel34.DataBindings.Add("Text", dt, "ARR_CEN_POST_NM");
            xrLabel35.DataBindings.Add("Text", dt, "DLVR_POST_CD");
            xrLabel36.DataBindings.Add("Text", dt, "DLVR_POST_NM");
            xrLabel37.DataBindings.Add("Text", dt, "DLVR_POST_DTL_CD");

            //보낸분
            xrLabel11.DataBindings.Add("Text", dt, "SNDR_ADDR");
            xrLabel12.DataBindings.Add("Text", dt, "SNDR_NM");
            xrLabel16.DataBindings.Add("Text", dt, "SNDR_ZIP");
            xrLabel20.DataBindings.Add("Text", dt, "SNDR_TEL");
            xrLabel24.DataBindings.Add("Text", dt, "SNDR_TEL_ETC");

            //받는분
            xrLabel13.DataBindings.Add("Text", dt, "ADDR");
            xrLabel14.DataBindings.Add("Text", dt, "NM");
            xrLabel22.DataBindings.Add("Text", dt, "VIR_TEL");
            xrLabel7.DataBindings.Add("Text", dt, "INV_NUM");

            xrBarCode2.DataBindings.Add("Text", dt, "INV_NUM_SUB");
            xrLabel10.DataBindings.Add("Text", dt, "LOCKER_NUM");

            xrLabel27.DataBindings.Add("Text", dt, "ADDR");
            xrBarCode3.DataBindings.Add("Text", dt, "INV_NUM_SUB");
            xrLabel28.DataBindings.Add("Text", dt, "NM");
            xrLabel29.DataBindings.Add("Text", dt, "VIR_TEL");
            xrLabel31.DataBindings.Add("Text", dt, "INV_NUM");
            xrLabel30.DataBindings.Add("Text", dt, "PDT_LIST");

            // 배송요청사항
            xrLabel40.DataBindings.Add("Text", dt, "MSG");
        }

        private void xrLabel25_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrLabel25.Text = xrLabel25.Text.Replace(@"\r\n", Environment.NewLine);
        }

        private void xrLabel30_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrLabel30.Text = xrLabel30.Text.Replace(@"\r\n", " ");
        }
    }
}
