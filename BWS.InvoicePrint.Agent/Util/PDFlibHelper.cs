﻿using PDFlib_dotnet;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BWS.InvoicePrint.Agent.Util
{
    class PDFlibHelper
    {
        public static void InvoiceDataToPrintData(string filePath, DataTable dataTable)
        {
            var pageWidth = 210d;
            var pageHeight = 297d;
            var cropWidth = 95.97d;
            var cropHeight = 92.964d;
            var leftPadding = 6d;
            var bottomPadding = 8d;

            using (var p = new PDFlib())
            {
                p.set_option("license=W900602-010551-801084-EB3T22-KKHYF2");
                p.set_option("errorpolicy=return");

                if (p.begin_document(filePath, "destination={type=fitwindow} pagelayout=singlepage") == -1)
                    throw new Exception(p.get_errmsg());

                //6조각으로 분리
                for (var seq = 1; seq <= dataTable.Rows.Count;)
                {
                    p.begin_page_ext(pageWidth * 2.83464567d, pageHeight * 2.83464567d, "");

                    for (var y = 2; y >= 0; y--)
                    {
                        for (var x = 0; x < 2; x++)
                        {
                            if (dataTable.Rows.Count < seq)
                                continue;

                            var row = dataTable.Rows[seq - 1];
                            var file = row["FILE_PATH"].ToString();
                            var posX = leftPadding + (cropWidth * x);
                            var posY = bottomPadding + (cropHeight * y);

                            var doc = p.open_pdi_document(file, "");
                            var page = p.open_pdi_page(doc, 1, "");

                            p.fit_pdi_page(page, posX * 2.83464567d, posY * 2.83464567d, "");

                            p.close_pdi_page(page);
                            p.close_pdi_document(doc);

                            seq++;
                        }
                    }

                    p.end_page_ext("");
                }

                p.end_document("");
            }
        }
    }
}
