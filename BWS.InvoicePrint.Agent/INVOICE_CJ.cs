﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace InvoicePrint
{
    public partial class INVOICE_CJ : DevExpress.XtraReports.UI.XtraReport
    {
        public INVOICE_CJ(DataTable dt)
        {
            InitializeComponent();

            this.DataSource = dt;

            // 송장번호
            xrLabel1.DataBindings.Add("Text", dt, "INV_NUM_DASH"); // 왼쪽
            xrLabel3.DataBindings.Add("Text", dt, "INV_NUM_DASH"); // 오른쪽
            // 접수일자
            xrLabel2.DataBindings.Add("Text", dt, "INSERT_DATE"); // 왼쪽
            xrLabel4.DataBindings.Add("Text", dt, "INSERT_DATE"); // 오른쪽
            // 바코드
            xrBarCode1.DataBindings.Add("Text", dt, "DLVCLSFCDFULL"); // 왼쪽 위 2T27
            xrBarCode3.DataBindings.Add("Text", dt, "INV_NUM"); // 왼쪽 아래     송장번호
            xrBarCode2.DataBindings.Add("Text", dt, "INV_NUM"); // 오른쪽 아래   송장번호
            // 지역코드(왼쪽 상단)
            xrLabel5.DataBindings.Add("Text", dt, "DLVCLSFCDPRE"); // 2
            xrLabel6.DataBindings.Add("Text", dt, "DLVCLSFCD"); // T27
            xrLabel7.DataBindings.Add("Text", dt, "DLVSUBCLSFCD"); // 0            
            // 운임구분(왼쪽 바코드 아래 옆)
            xrLabel12.DataBindings.Add("Text", dt, "FARE_DIV"); // 선불
            // 운송장번호(송화인 영수증 아래)
            xrLabel13.DataBindings.Add("Text", dt, "INV_NUM_DASH"); // 6239-9183-4641
            // 접수일자(운임구분 아래)
            xrLabel14.DataBindings.Add("Text", dt, "INSERT_DATE"); // 2019.07.05
            // 보내는분(왼쪽 운송장번호 아래)
            xrLabel15.DataBindings.Add("Text", dt, "SNDR_NM_TEL_HP"); // 레드프린팅 / 1544-6698
            // 받는분(왼쪽 보내는분 아래)
            xrLabel16.DataBindings.Add("Text", dt, "RCVR_NM_TEL_HP"); // 레드프린팅 / 1544-6698
            // 주소(왼쪽 받는분 아래)
            xrLabel17.DataBindings.Add("Text", dt, "RCVR_ADDR"); // 서울 성동구 성수동 아차산로 11가길 6 1층 [성수동2가 278-33]
            // Note(왼쪽 주소 아래)
            xrLabel18.DataBindings.Add("Text", dt, "PDT_NM"); // 150900356561, 아크릴명찰(행번[1번]-1건)
            // 사물함번호(왼쪽 Note 아래)
            xrLabel33.DataBindings.Add("Text", dt, "LOCKER_NUM"); // No:999
            // 운송자명 등 정보(왼쪽 맨 하단)
            xrLabel34.DataBindings.Add("Text", dt, "DLVPREARRBRANNM"); // 서울북성수
            xrLabel35.DataBindings.Add("Text", dt, "DLVPREARREMPNM"); // 강인수
            xrLabel36.DataBindings.Add("Text", dt, "DLVPREARREMPNICKNM"); // F-58
            // 받는분 (오른쪽 맨 상단)
            xrLabel8.DataBindings.Add("Text", dt, "RCVRCLSFADDR"); // 성수2 278-33
            xrLabel9.DataBindings.Add("Text", dt, "RCVR_NM"); // 레드프린팅
            xrLabel44.DataBindings.Add("Text", dt, "RCVR_TEL_HP"); // 1544-6698
            xrLabel10.DataBindings.Add("Text", dt, "RCVR_ADDR"); // 서울 성동구 성수동 아차산로 11가길 6 1층 [성수동 2가 278-33]
            // 보내는분 (오른쪽 맨 상단 아래)
            xrLabel19.DataBindings.Add("Text", dt, "SNDR_NM"); // 레드프린팅
            xrLabel20.DataBindings.Add("Text", dt, "SNDR_TEL_HP"); // 1544-6698
            xrLabel21.DataBindings.Add("Text", dt, "DLVR_REQ"); // 테스트하기 넘넘 힘드네용요요요용요용요~ㅜㅜ
            // 박스수량, 운임, 정산구분 (오른쪽 중간)
            xrLabel22.DataBindings.Add("Text", dt, "BOXTYP"); // 극소 B
            xrLabel23.DataBindings.Add("Text", dt, "DEALFARE"); // 0
            xrLabel24.DataBindings.Add("Text", dt, "FARE_DIV"); // 선불
            // 받는분 (오른쪽 하단)
            xrLabel25.DataBindings.Add("Text", dt, "PACKING_NUM"); // P1907050638
            xrLabel26.DataBindings.Add("Text", dt, "RCVR_NM_ENC"); // 레드*
            xrLabel27.DataBindings.Add("Text", dt, "RCVR_TEL_HP_ENC"); // 1544-****
            xrLabel28.DataBindings.Add("Text", dt, "RCVR_ADDR"); // 서울 성동구 성수동 아차산로 11가길 6 1층 [성수동2가 278-33]
            // 보내는분 (오른쪽 맨 하단)
            xrLabel29.DataBindings.Add("Text", dt, "SNDR_NM"); // 레드프린팅
            xrLabel32.DataBindings.Add("Text", dt, "FARE_DIV"); // 선불
            xrLabel30.DataBindings.Add("Text", dt, "SNDR_TEL"); // 1544-6698
            xrLabel31.DataBindings.Add("Text", dt, "SNDR_ADDR"); // 서울 성동구 성수동 아차산로 11가길 6 3층 [성수동1가]
        }

    }
}
