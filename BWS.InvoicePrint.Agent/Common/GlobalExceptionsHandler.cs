﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using TheOne.Diagnostics;

namespace BWS.InvoicePrint.Agent.Common
{
    class GlobalExceptionsHandler
    {
        public static void AddHandler()
        {
            AppDomain.CurrentDomain.UnhandledException += ConsoleThreadException;
            Application.ThreadException += UIThreadException;
        }

        private static void UIThreadException(object sender, ThreadExceptionEventArgs e)
        {
            ShowException(e.Exception as Exception);
        }

        private static void ConsoleThreadException(object sender, UnhandledExceptionEventArgs e)
        {
            ShowException(e.ExceptionObject as Exception);
        }

        static void ShowException(Exception ex)
        {
            IFoxLog log = FoxLogManager.GetLogger("BWS.InvoicePrint.Agent");
            log.Write(ex.Message, FoxLogLevel.Error, ex);
        }
    }
}
