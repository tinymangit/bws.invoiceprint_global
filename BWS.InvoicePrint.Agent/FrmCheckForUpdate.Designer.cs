﻿
namespace BWS.InvoicePrint.Agent
{
    partial class FrmCheckForUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.automaticUpdater1 = new wyDay.Controls.AutomaticUpdater();
            ((System.ComponentModel.ISupportInitialize)(this.automaticUpdater1)).BeginInit();
            this.SuspendLayout();
            // 
            // automaticUpdater1
            // 
            this.automaticUpdater1.ContainerForm = this;
            this.automaticUpdater1.GUID = "A2C012D2-B167-43BA-AF49-9D0DA59E5792";
            this.automaticUpdater1.Location = new System.Drawing.Point(36, 27);
            this.automaticUpdater1.Name = "automaticUpdater1";
            this.automaticUpdater1.Size = new System.Drawing.Size(16, 16);
            this.automaticUpdater1.TabIndex = 0;
            this.automaticUpdater1.wyUpdateCommandline = null;
            // 
            // FrmCheckForUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(364, 71);
            this.Controls.Add(this.automaticUpdater1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCheckForUpdate";
            this.ShowIcon = false;
            this.Text = "업데이트 확인";
            this.Load += new System.EventHandler(this.FrmCheckForUpdate_Load);
            this.Shown += new System.EventHandler(this.FrmCheckForUpdate_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.automaticUpdater1)).EndInit();
            this.ResumeLayout(false);
        }

        

        #endregion

        private wyDay.Controls.AutomaticUpdater automaticUpdater1;

    }
}