﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BWS.InvoicePrint.Agent.Controls
{
    public partial class InvoicePalette : UserControl
    {
        private List<string> Items { get; set; }
        private List<Label> Labels { get; set; }

        public bool IsEmpty { get { return this.Items.Count < 6; } }

        public InvoicePalette()
        {
            InitializeComponent();
            InitializeControls();
        }

        private void InitializeControls()
        {
            this.Items = new List<string>();
            this.Labels = new List<Label>();

            this.label1.ResetText();
            this.label2.ResetText();
            this.label3.ResetText();
            this.label4.ResetText();
            this.label5.ResetText();
            this.label6.ResetText();

            this.Labels.AddRange(new[] { label1, label2, label3, label4, label5, label6 });
        }

        public void Push(string orderNum)
        {
            if (Items.Count >= 6)
                return;

            this.Labels[Items.Count].Text = orderNum;
            this.Items.Add(orderNum);
        }

    }
}
