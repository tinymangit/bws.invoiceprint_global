﻿using System;
using System.Windows.Forms;
using System.Threading;

using BWS.InvoicePrint.Agent.Common;
using System.IO;

namespace BWS.InvoicePrint.Agent
{
    static class Program
    {
        static Mutex m;
        static string _version;

        internal static string CurrentVersion
        {
            get
            {
                return _version;
            }
            set
            {
                SetVerion(value);
            }
        }

        private static string VERSION_FILE = "InvoiceAgent.ver";

        /// <summary>
        /// 해당 응용 프로그램의 주 진입점입니다.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                // 중복 실행 방지..
                bool first = false;
                m = new Mutex(true, Application.ProductName.ToString(), out first);
                _version = GetVersion();
                if ((first))
                {
                    GlobalExceptionsHandler.AddHandler();

                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    // 라이선스 정보
                    ceTe.DynamicPDF.Printing.PrintJob.AddLicense("PMG30NXDDNGOENYSU4ra9E/7U0UV4Q2Y/9EnTDnmXQy1qZhxSNaOk/lLX3W/yh/S9yOzZ0P4l7PqiQc1/EoiGi0KzbGhKo03IsRw");
                    Application.Run(new FrmCheckForUpdate());

                    m.ReleaseMutex();
                }

                //string json = "{ \"lockerName\": \"Locker01\", \"printAgentName\" : \"PrinterAgent01\" }";
                //var obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Core.SubscribeLockerMessage>(json);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                throw;
            }
        }

        /// <summary>
        /// 파일에 쓰여진 버전 정보 반환
        /// </summary>
        /// <returns></returns>
        private static string GetVersion()
        {
            string ver = "0.0.0";
            string filePath = Path.Combine(Application.ExecutablePath, VERSION_FILE);

            if(File.Exists(filePath))
                ver = File.ReadAllText(filePath);

            return string.IsNullOrEmpty(ver) ? "0.0.0" : ver;
        }

        /// <summary>
        /// 업데이트 처리된 버전을 파일에 저장
        /// </summary>
        /// <param name="ver"></param>
        private static void SetVerion(string ver)
        {
            string filePath = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), VERSION_FILE);
            File.WriteAllText(filePath, ver);

            _version = ver;
        }
    }
}
