﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace BWS.InvoicePrint.Core
{
    public class SubscribeLockerMessage
    {
        public SubscribeLockerMessage()
        {
            this.IsSuccess = false;
            this.ErrorMessage = string.Empty;
        }

        [JsonProperty(PropertyName = "lockerName")]
        public string LockerName { get; set; }

        [JsonProperty(PropertyName = "printAgentName")]
        public string PrintAgentName { get; set; }

        public bool IsSuccess { get; set; }

        public string ErrorMessage { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
