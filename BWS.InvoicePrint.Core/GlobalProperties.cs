﻿namespace BWS.InvoicePrint.Core
{
    /// <summary>
    /// 출력 요청부터 처리 결과까지의 상태 구분
    /// </summary>
    public enum PrintState
    {
        /// <summary>
        /// 최초 보관함(WEB)에서 서버로 출력 요청
        /// </summary>
        PRT_FROM_LOCKER = 1,
        /// <summary>
        /// 서버에서 해당 에이전트로 출력 요청
        /// </summary>
        PRT_REQ_AGT,
        /// <summary>
        /// 에이전트에서 실제 출력 직전 상태
        /// </summary>
        PRT_AGENT,
        /// <summary>
        /// 에이전트에서 출력이 완료되고 서버에 완료 응답전 상태 (결과 관계 없음)
        /// </summary>
        COMP_TO_SERV,
        /// <summary>
        /// 출력요청에 대한 처리 완료 (결과 관계 없음)
        /// </summary>
        PRT_COMPLETE,
    }
}
