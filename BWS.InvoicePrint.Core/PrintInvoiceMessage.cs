﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

using Newtonsoft.Json;

namespace BWS.InvoicePrint.Core
{
    public class PrintInvoiceMessage
    {
        public PrintInvoiceMessage()
        {
            this.IsSuccess = false;
            this.ErrorMessage = string.Empty;
            this.InvoiceList = null;
        }

        [JsonProperty(PropertyName = "lockerName")]
        public string LockerName { get; set; }

        [JsonProperty(PropertyName = "companyCode")]
        public string ComapnyCode { get; set; }

        [JsonProperty(PropertyName = "languageCode")]
        public string LanguageCode { get; set; }

        [JsonProperty(PropertyName = "packingNumber")]
        public string PackingNumber { get; set; }

        [JsonProperty(PropertyName = "type")]
        public string InvType { get; set; }

        [JsonProperty(PropertyName = "orderList")]
        public List<OrderInfo> OrderNumbers { get; set; }

        public bool IsSuccess { get; set; }

        public string ErrorMessage { get; set; }

        public DataTable InvoiceList { get; set; }

        public DataTable InvoiceOrdNatGbList { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }        
    }

    public class OrderInfo
    {
        [JsonProperty(PropertyName = "ORD_NUM")]
        public string OrderNo { get; set; }
    }
}
